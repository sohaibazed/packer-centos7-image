# Packer Example - CentOS 7 minimal Vagrant Box


**Pre-built Vagrant Box**:

This example packer configuration file will install and configure Centos 7 using shell scripts, generate a Vagrant box file for Virtualbox and upload box file to app.vagrantup.com

## Requirements

The following software must be installed/present on your local machine before you can use Packer to build the Vagrant box file:

  - [Packer](http://www.packer.io/)
  - [Vagrant](http://vagrantup.com/)
  - [VirtualBox](https://www.virtualbox.org/) (if you want to build the VirtualBox box)
  - [Centos 7] (https://www.centos.org/download/)

## Usage

Downlaod Centos image inside the directory
Make sure all the required software (listed above) are installed
Run

    $ packer build -var 'version=1.0' centos7.json

After a few minutes, Packer should tell you the box was generated successfully, and the box was uploaded to Vagrant Cloud.

> **Note**: Make sure to add vagrant cloud token in centos7.json file, so that packer script can upload the box image to vagrant cloud

## Testing built boxes

There's an included Vagrantfile that allows quick testing of the built Vagrant boxes. From this same directory, run one the following command after building the box:

    $ vagrant up
