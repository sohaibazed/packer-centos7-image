#!/bin/bash -eux

#login as root
sudo su

# Configure sshd
sudo sed -e '/PermitRootLogin/ s/^#*/#/' -i /etc/ssh/sshd_config
sudo sed '/^#PermitRootLogin/a PermitRootLogin yes' -i /etc/ssh/sshd_config
sudo sed -i '/^#ListenAddress 0.0.0.0/s/^#//' -i /etc/ssh/sshd_config
sudo sed '/^#PasswordAuthentication/a PasswordAuthentication yes' -i /etc/ssh/sshd_config
sudo systemctl restart sshd
echo "root:Juniper123" | sudo chpasswd
sudo iptables -F

# Install Packages.
sudo yum update -y 
sudo yum install -y epel-release
sudo yum install -y ntp git
sudo yum install -y python-urllib3 git ansible-2.4.2.0 pciutils wget tcpdump net-tools python-pip python-boto python2-boto3

# Installing vagrant keys
mkdir /home/vagrant/.ssh
chown -R vagrant:vagrant /home/vagrant/.ssh
chmod 700 /home/vagrant/.ssh
cd /home/vagrant/.ssh
wget --no-check-certificate 'https://raw.github.com/mitchellh/vagrant/master/keys/vagrant.pub' -O authorized_keys
chmod 600 /home/vagrant/.ssh/authorized_keys
chown vagrant:vagrant /home/vagrant/.ssh/authorized_keys


# configure NTP
sudo rm -rf /etc/ntp.conf
sudo touch /etc/ntp.conf
sudo echo "driftfile /var/lib/ntp/drift" >> /etc/ntp.conf
sudo echo "server pool.ntp.org" >> /etc/ntp.conf
sudo echo "restrict 127.0.0.1" >> /etc/ntp.conf
sudo echo "restrict -6 ::1" >> /etc/ntp.conf
sudo echo "includefile /etc/ntp/crypto/pw" >> /etc/ntp.conf
sudo echo "keys /etc/ntp/keys" >> /etc/ntp.conf
sudo systemctl start ntpd
sudo systemctl enable ntpd

systemctl disable NetworkManager
systemctl enable network
systemctl stop NetworkManager
systemctl start network
